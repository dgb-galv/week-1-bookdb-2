from django.db import models

# Create your models here.

class Book(models.Model):
    title = models.CharField(max_length=75)
    publish_date = models.DateField()
    description = models.TextField()
