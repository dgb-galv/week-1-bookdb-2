from django.shortcuts import render

from books.forms import BookForm
from books.models import Book

# Create your views here.
def show_books(request):
    if request.method == "POST":
        form = BookForm(request.POST)
        if form.is_valid:
            form.save()
    books = Book.objects.all()
    form = BookForm()
    context = {
        "books": books,
        "form": form,
    }
    return render(request, "home.html", context)
